/* Tomamos todo los elementos  */
var inputTarea = document.getElementById("tarea");
var btn = document.getElementById("agregar");
var listado = document.getElementById("listado");
var cantidad = document.getElementById("cantidad");

/* Variable que lleva la cantidad de tareas */
var total = 0;

/* Funcion que agrega la tarea al listado */
btn.onclick = async function (){
    /* controlamos si el campo input esta vacio */
    if (inputTarea.value == "") {
        alert("El campo está vacio");
        return ;
    }
    /* Tomamos el valor del campo */
    var elemento = inputTarea.value;
    /* Crear elemento li */
    var li = document.createElement("li")
    li.textContent = elemento;
    /* Agregamos el li al listado */
    listado.appendChild(li);

    /* Incrementamos la cantidad de tareas */
    total ++;
    cantidad.innerHTML = total;

    /* Agregamos el boton eliminar a cada elemento */
    var btnEliminar = document.createElement("span");
    btnEliminar.textContent = "\u00d7";
    li.appendChild(btnEliminar);

    /* Agregamos la funcionalidad que elimna la tarea del listado */
    btnEliminar.onclick = function (){
        li.remove();
        total --;
        cantidad.innerHTML = total;
    }

    /* Limpiamos el campo input */
    inputTarea.value = "";
}